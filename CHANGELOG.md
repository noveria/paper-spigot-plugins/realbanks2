# RealBanks Changelog

## RealBanks v1.7.7
* MySQL Connection Fix
* Prefix in Console-Messages

## RealBanks v1.7.4
* Fixed minor bug in SQLite & MySQL integration

## RealBanks v1.7.3
* Fixed Regex Integer & Double check

## RealBanks v1.7.1
* Changed Message-Colour
* Added Prefixes in Messages

## RealBanks v1.7.0
* Added Command to show, withdraw & deposit money on other players bank account

## RealBanks v1.6.0
* Added `getAllMoney();` Method

## RealBanks v1.3.3
* Added Support for Crimson and Warped Signs
* Added TabCompletion.

## RealBanks v1.3.2
* Replaced standard statements with prepared statements to transferMoney() for more security
* removed ability to set custom money inside atm (Bug!)
