package me.linuxsquare.realbanks2.view;

import me.linuxsquare.realbanks2.RealBanks;
import me.linuxsquare.realbanks2.model.LanguageModel;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ATMView {

    private RealBanks plugin = RealBanks.getPlugin();

    private LanguageModel lm = plugin.getLanguageModel();
    private FileConfiguration lang = lm.get();

    private final String GUI_NAME = "§6" + lang.getString("ATM.atm");

    public ATMView() {

    }

    public void openGUI(Player player) {
        Inventory inv = Bukkit.createInventory(null, 9 * 3, getGUI_NAME());
        inv.setItem(0, CustomHeads.customHead(CustomHeads.BITCOIN, "0"));
        inv.setItem(1, new ItemStack(Material.IRON_BARS));
        inv.setItem(3, CustomHeads.customHead(CustomHeads.LIME_PLUS, "+20"));
        inv.setItem(4, CustomHeads.customHead(CustomHeads.LIME_PLUS, "+50"));
        inv.setItem(5, CustomHeads.customHead(CustomHeads.LIME_PLUS, "+100"));
        inv.setItem(6, CustomHeads.customHead(CustomHeads.LIME_PLUS, "+200"));
        inv.setItem(7, CustomHeads.customHead(CustomHeads.LIME_PLUS, "+500"));
        inv.setItem(9, CustomHeads.customHead(CustomHeads.CHECKMARK_ACTIVE, "Deposit"));
        inv.setItem(10, new ItemStack(Material.IRON_BARS));
        inv.setItem(18, CustomHeads.customHead(CustomHeads.CROSS_ACTIVE, "Withdraw"));
        inv.setItem(19, new ItemStack(Material.IRON_BARS));
        inv.setItem(21, CustomHeads.customHead(CustomHeads.RED_MINUS, "-20"));
        inv.setItem(22, CustomHeads.customHead(CustomHeads.RED_MINUS, "-50"));
        inv.setItem(23, CustomHeads.customHead(CustomHeads.RED_MINUS, "-100"));
        inv.setItem(24, CustomHeads.customHead(CustomHeads.RED_MINUS, "-200"));
        inv.setItem(25, CustomHeads.customHead(CustomHeads.RED_MINUS, "-500"));
        inv.setItem(26, CustomHeads.customHead(CustomHeads.TRASH_CAN, "Reset"));
        player.openInventory(inv);
    }

    public String getGUI_NAME() {
        return this.GUI_NAME;
    }



}
