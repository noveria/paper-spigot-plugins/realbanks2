package me.linuxsquare.realbanks2.view;

import com.destroystokyo.paper.profile.PlayerProfile;
import com.destroystokyo.paper.profile.ProfileProperty;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.UUID;

public enum CustomHeads {

    BITCOIN("e771f93b-4673-4704-9ae5-77dfdc8bb863", "BitCoin",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYWNkNzBjZTQ4MTg1ODFjYTQ3YWRmNmI4MTY3OWZkMTY0NmZkNjg3YzcxMjdmZGFhZTk0ZmVkNjQwMTU1ZSJ9fX0="),
    LIME_PLUS("a18f6c08-2311-4f8c-a69c-2563148a74f3", "Lime Plus",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYjA1NmJjMTI0NGZjZmY5OTM0NGYxMmFiYTQyYWMyM2ZlZTZlZjZlMzM1MWQyN2QyNzNjMTU3MjUzMWYifX19"),
    RED_MINUS("6e79d58c-f622-4a11-8852-8b9f1e3ea6cc", "Red Minus",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNGU0YjhiOGQyMzYyYzg2NGUwNjIzMDE0ODdkOTRkMzI3MmE2YjU3MGFmYmY4MGMyYzViMTQ4Yzk1NDU3OWQ0NiJ9fX0="),
    CHECKMARK_ACTIVE("b4e096c1-65b2-4abd-bedc-e67842bb42b1", "Checkmark (active)",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvY2UyYTUzMGY0MjcyNmZhN2EzMWVmYWI4ZTQzZGFkZWUxODg5MzdjZjgyNGFmODhlYThlNGM5M2E0OWM1NzI5NCJ9fX0="),
    CROSS_ACTIVE("eab805c4-ba36-4baa-841f-35031d956453", "Cross (active)",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZTljZGI5YWYzOGNmNDFkYWE1M2JjOGNkYTc2NjVjNTA5NjMyZDE0ZTY3OGYwZjE5ZjI2M2Y0NmU1NDFkOGEzMCJ9fX0="),
    TRASH_CAN("874ae835-78a1-4bcf-9d0d-a9ee24e554ee", "Trash Can",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNDQxZjRlZGJjNjhjOTA2MTM1NTI0MmJkNzNlZmZjOTI5OWEzMjUyYjlmMTFlODJiNWYxZWM3YjNiNmFjMCJ9fX0="),
    MONITOR("79f5d57a-7903-4557-a681-06188330825f", "Monitor",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMWRjMWQzMGM2YTY1MTcxMjhkN2JiN2EyOWU3YWM3YWM1NmQxZmJlMjA3NmYzMDhkZmQ5Y2E1M2NmMmJmODdjNiJ9fX0="),
    GOLDEN_ARROW("f44959bf-7f92-4a49-9517-8ec2cfeaf006", "Golden Arrow",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNWE5MmFkNDU2Zjc2ZWM3Y2FhMzU5NTkyMmQ1ZmNjMzhkY2E5MjhkYzY3MTVmNzUyZTc0YzhkZGRlMzQ0ZSJ9fX0=");

    private CustomHeads(String ownerUUID, String ownerName, String textureProperty) {
        this.ownerUUID = ownerUUID;
        this.ownerName = ownerName;
        this.textureProperty = textureProperty;
    }

    private String ownerUUID;
    private String ownerName;
    private String textureProperty;

    public String getOwnerUUID() {
        return ownerUUID;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getTextureProperty() {
        return textureProperty;
    }

    public static ItemStack customHead(CustomHeads cs, String displayName) {
        UUID ownerUUID = UUID.fromString(cs.getOwnerUUID());

        ItemStack stack = new ItemStack(Material.PLAYER_HEAD);
        SkullMeta meta = (SkullMeta) stack.getItemMeta();
        PlayerProfile profile = Bukkit.createProfile(ownerUUID, cs.getOwnerName());
        profile.setProperty(new ProfileProperty("textures", cs.getTextureProperty()));
        meta.setPlayerProfile(profile);
        meta.setDisplayName(displayName);
        stack.setItemMeta(meta);
        return stack;
    }

}
