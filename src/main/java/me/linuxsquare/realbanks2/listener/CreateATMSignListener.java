package me.linuxsquare.realbanks2.listener;

import me.linuxsquare.realbanks2.RealBanks;
import me.linuxsquare.realbanks2.model.LanguageModel;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

public class CreateATMSignListener implements Listener {

    private RealBanks plugin = RealBanks.getPlugin();

    private LanguageModel lm = plugin.getLanguageModel();
    private FileConfiguration lang = lm.get();

    @EventHandler
    public void onSignChange(SignChangeEvent e) {
        Player p = e.getPlayer();
        if (p.hasPermission("rb.createatm")) {
            if (e.getLine(0).equalsIgnoreCase("rb") || e.getLine(0).equalsIgnoreCase("realbanks")
                    || e.getLine(0).equalsIgnoreCase("[rb]") || e.getLine(0).equalsIgnoreCase("[realbanks]")) {
                if (e.getLine(1).equalsIgnoreCase("atm")) {
                    e.setLine(0, ChatColor.GREEN + "[RealBanks]");
                    Block block = e.getBlock();
                    if (block.getType().equals(Material.DARK_OAK_WALL_SIGN)
                            || block.getType().equals(Material.DARK_OAK_SIGN)
                            || block.getType().equals(Material.CRIMSON_SIGN)
                            || block.getType().equals(Material.CRIMSON_WALL_SIGN)
                            || block.getType().equals(Material.WARPED_SIGN)
                            || block.getType().equals(Material.WARPED_WALL_SIGN)) {
                        e.setLine(1, ChatColor.WHITE + "ATM");
                    } else {
                        e.setLine(1, "ATM");
                    }
                    if(lang.contains(p.getLocale().toLowerCase())) {
                        p.sendMessage(RealBanks.PREFIX + lang.getString(p.getLocale().toLowerCase()+".ATM.successfullycreated"));
                    } else {
                        p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.ATM.successfullycreated"));
                    }
                }
            }
        }
    }

}
