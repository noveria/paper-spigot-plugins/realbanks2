package me.linuxsquare.realbanks2.listener;

import me.linuxsquare.realbanks2.RealBanks;
import me.linuxsquare.realbanks2.model.LanguageModel;
import me.linuxsquare.realbanks2.view.CustomHeads;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class TypeMoneyListener implements Listener {

    private RealBanks plugin = RealBanks.getPlugin();

    private LanguageModel lm = plugin.getLanguageModel();
    private FileConfiguration lang = lm.get();

    private HashMap<Player, Inventory> wait4Play = new HashMap<Player, Inventory>();

    public TypeMoneyListener() {
    }

    public TypeMoneyListener(Player p, Inventory inv) {
        wait4Play.put(p, inv);
        if(wait4Play.containsKey(p)) {
            System.out.println(wait4Play.get(p));
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        Player p = event.getPlayer();

        if (wait4Play.containsKey(p)) {
            System.out.println("Contains2");
            String input = event.getMessage();
            System.out.println(input);
            event.setCancelled(true);
            if (input.matches("[0-9]+")) {
                Bukkit.getScheduler().runTask(plugin, () -> {
                    p.openInventory(wait4Play.get(p));
                    wait4Play.get(p).setItem(0, new ItemStack(Material.AIR));
                    wait4Play.get(p).setItem(0, CustomHeads.customHead(CustomHeads.BITCOIN, input));
                });
            } else {
                p.sendMessage(plugin.getConfig().getString("Settings.prefix") + ChatColor.translateAlternateColorCodes('&',
                        lang.getString("ATM.Errors.onlynumeric")));
            }
            wait4Play.remove(p);
        }
    }

}
