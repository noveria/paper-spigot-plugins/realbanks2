package me.linuxsquare.realbanks2.listener;

import me.linuxsquare.realbanks2.RealBanks;
import me.linuxsquare.realbanks2.model.LanguageModel;
import me.linuxsquare.realbanks2.view.ATMView;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class ATMGuiListener implements Listener {

    private RealBanks plugin = RealBanks.getPlugin();

    private LanguageModel lm = plugin.getLanguageModel();
    private FileConfiguration lang = lm.get();

    private ATMView av = new ATMView();

    @EventHandler
    public void onATMClicked(PlayerInteractEvent e) {
        Action action = e.getAction();
        Block block = e.getClickedBlock();

        if (action.equals(Action.RIGHT_CLICK_BLOCK)) {
            if (block.getType().equals(Material.OAK_WALL_SIGN) || block.getType().equals(Material.OAK_SIGN)
                    || block.getType().equals(Material.SPRUCE_WALL_SIGN) || block.getType().equals(Material.SPRUCE_SIGN)
                    || block.getType().equals(Material.BIRCH_WALL_SIGN) || block.getType().equals(Material.BIRCH_SIGN)
                    || block.getType().equals(Material.JUNGLE_WALL_SIGN) || block.getType().equals(Material.JUNGLE_SIGN)
                    || block.getType().equals(Material.ACACIA_WALL_SIGN) || block.getType().equals(Material.ACACIA_SIGN)
                    || block.getType().equals(Material.DARK_OAK_WALL_SIGN)
                    || block.getType().equals(Material.DARK_OAK_SIGN)
                    || block.getType().equals(Material.CRIMSON_SIGN)
                    || block.getType().equals(Material.CRIMSON_WALL_SIGN)
                    || block.getType().equals(Material.WARPED_SIGN)
                    || block.getType().equals(Material.WARPED_WALL_SIGN)) {
                Sign s = (Sign) block.getState();
                if (s.getLine(0).contains(ChatColor.GREEN + "[RealBanks]") && s.getLine(1).contains("ATM")) {
                    if (e.getPlayer().hasPermission("rb.useatm")) {
                        av.openGUI(e.getPlayer());
                    } else {
                        if(lang.contains(e.getPlayer().getLocale().toLowerCase())) {
                            e.getPlayer().sendMessage(RealBanks.PREFIX + lang.getString(e.getPlayer().getLocale().toLowerCase()+".General.Errors.nopermission"));
                        } else {
                            e.getPlayer().sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.nopermission"));
                        }

                    }
                }
            }
        }

    }


}
