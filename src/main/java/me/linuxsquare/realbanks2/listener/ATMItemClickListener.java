package me.linuxsquare.realbanks2.listener;

import me.linuxsquare.realbanks2.RealBanks;
import me.linuxsquare.realbanks2.model.DatabaseModel;
import me.linuxsquare.realbanks2.model.LanguageModel;
import me.linuxsquare.realbanks2.view.ATMView;
import me.linuxsquare.realbanks2.view.CustomHeads;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

public class ATMItemClickListener implements Listener {

    private RealBanks plugin = RealBanks.getPlugin();

    private DatabaseModel dm = plugin.getDatabaseModel();

    private FileConfiguration conf = plugin.getConfig();

    private LanguageModel lm = plugin.getLanguageModel();
    private FileConfiguration lang = lm.get();

    private ATMView av = new ATMView();

    private Inventory current;
    private Player p;

    @EventHandler
    public void onATMGUIItemClicked(InventoryClickEvent e) {
        if (!(e.getWhoClicked() instanceof Player))
            return;
        setPlayer((Player) e.getWhoClicked());
        try {
            if (e.getView().getTitle().equals(av.getGUI_NAME())) {
                e.setCancelled(true);
                setInventory(e.getClickedInventory());
                    switch (e.getCurrentItem().getItemMeta().getDisplayName()) {
                        case "+20":
                            addMoneytoBTC(current, 20, p);
                            break;
                        case "+50":
                            addMoneytoBTC(current, 50, p);
                            break;
                        case "+100":
                            addMoneytoBTC(current, 100, p);
                            break;
                        case "+200":
                            addMoneytoBTC(current, 200, p);
                            break;
                        case "+500":
                            addMoneytoBTC(current, 500, p);
                            break;
                        case "-20":
                            rmMoneyfromBTC(current, 20, p);
                            break;
                        case "-50":
                            rmMoneyfromBTC(current, 50, p);
                            break;
                        case "-100":
                            rmMoneyfromBTC(current, 100, p);
                            break;
                        case "-200":
                            rmMoneyfromBTC(current, 200, p);
                            break;
                        case "-500":
                            rmMoneyfromBTC(current, 500, p);
                            break;
                        case "Reset":
                            resetMoneyBTC(current, p);
                            break;
                        case "Withdraw":
                            withdrawMoneyFromDB(p.getUniqueId(), p, current);
                            break;
                        case "Deposit":
                            depositMoneyToDB(p.getUniqueId(), p, current);
                            break;
                    }
            }
        } catch (Exception ignored) {}
    }

    private void addMoneytoBTC(Inventory inv, int summand, Player pl) {
        pl.playSound(pl.getLocation(), Sound.UI_BUTTON_CLICK, 1.0F, 1.0F);
        String oldvalue = inv.getItem(0).getItemMeta().getDisplayName();
        int intvalue = Integer.parseInt(oldvalue);
        String newvalue = Integer.toString(intvalue += summand);
        inv.setItem(0, new ItemStack(Material.AIR));
        inv.setItem(0, CustomHeads.customHead(CustomHeads.BITCOIN, newvalue));
    }

    private void rmMoneyfromBTC(Inventory inv, int subtrahend, Player pl) {
        pl.playSound(pl.getLocation(), Sound.UI_BUTTON_CLICK, 1.0F, 0.0F);
        String oldvalue = inv.getItem(0).getItemMeta().getDisplayName();
        int intvalue = Integer.parseInt(oldvalue);
        String newvalue = new String();
        if (intvalue - subtrahend <= 0) {
            newvalue = Integer.toString(0);
        } else {
            newvalue = Integer.toString(intvalue + -subtrahend);
        }
        inv.setItem(0, new ItemStack(Material.AIR));
        inv.setItem(0, CustomHeads.customHead(CustomHeads.BITCOIN, newvalue));
    }

    private void resetMoneyBTC(Inventory inv, Player pl) {
        pl.playSound(pl.getLocation(), Sound.ENTITY_ARROW_HIT_PLAYER, 1.0F, 0.0F);
        inv.setItem(0, new ItemStack(Material.AIR));
        inv.setItem(0, CustomHeads.customHead(CustomHeads.BITCOIN, "0"));
    }

    private void withdrawMoneyFromDB(UUID puid, Player p, Inventory inv) {

        String locale = p.getLocale().toLowerCase();

        String value = inv.getItem(0).getItemMeta().getDisplayName();
        int btcvalue = 0;
        try {
            btcvalue = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            if(lang.contains(locale)) {
                p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".ATM.Errors.overintvalue").replace("%balance%",
                        plugin.formatMoney(Integer.MAX_VALUE)));
            } else {
                p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.ATM.Errors.overintvalue").replace("%balance%",
                        plugin.formatMoney(Integer.MAX_VALUE)));
            }

            p.closeInventory();
            return;
        }
        try {
            Statement stmt = dm.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT money FROM Accounts WHERE uuid = '" + puid + "';");
            String money = new String();
            while (rs.next()) {
                money = rs.getString(1);
            }
            double intmoney = Double.parseDouble(money);
            if (intmoney - btcvalue < Math.abs(conf.getInt("Settings.MinimumMoney"))) {
                if(lang.contains(locale)) {
                    p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".ATM.Errors.notenoughmoney")
                            + "\n"
                            + lang.getString("General.balancecheck").replace("%balance%",
                            plugin.formatMoney(intmoney)));
                } else {
                    p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.ATM.Errors.notenoughmoney")
                            + "\n"
                            + lang.getString("General.balancecheck").replace("%balance%",
                            plugin.formatMoney(intmoney)));
                }

            } else {
                double newmoney = intmoney - btcvalue;
                money = Double.toString(newmoney);
                plugin.withdrawMoneyFromBank(puid, money);
                plugin.getEconomy().depositPlayer(Bukkit.getOfflinePlayer(puid), btcvalue);
                if(lang.contains(locale)) {
                    p.sendMessage(RealBanks.PREFIX +
                            lang.getString(locale+".ATM.success"));
                } else {
                    p.sendMessage(RealBanks.PREFIX +
                            lang.getString("en_gb.ATM.success"));
                }

                p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0F, 0.0F);
            }
            p.closeInventory();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void depositMoneyToDB(UUID puid, Player p, Inventory inv) {
        String value = inv.getItem(0).getItemMeta().getDisplayName();

        String locale = p.getLocale().toLowerCase();

        int btcvalue = Integer.parseInt(value);
        try {
            Statement stmt = dm.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT money FROM Accounts WHERE uuid = '" + puid + "';");
            String money = new String();
            while (rs.next()) {
                money = rs.getString(1);
            }
            double intmoney = new BigDecimal(money).doubleValue();
            if (plugin.getEconomy().getBalance(Bukkit.getOfflinePlayer(p.getUniqueId())) < btcvalue) {
                if(lang.contains(locale)) {
                    p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".ATM.Errors.notenoughmoneypocket")
                            + "\n"
                            + lang.getString(locale+".General.balancecheck").replace("%balance%",
                            plugin.formatMoney(plugin.getEconomy().getBalance(Bukkit.getOfflinePlayer(p.getUniqueId())))));
                } else {
                    p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.ATM.Errors.notenoughmoneypocket")
                            + "\n"
                            + lang.getString("en_gb.General.balancecheck").replace("%balance%",
                            plugin.formatMoney(plugin.getEconomy().getBalance(Bukkit.getOfflinePlayer(p.getUniqueId())))));
                }

            } else {
                double newmoney = intmoney + btcvalue;
                money = Double.toString(newmoney);
                plugin.depositMoneyToBank(puid, money);
                plugin.getEconomy().withdrawPlayer(Bukkit.getOfflinePlayer(p.getUniqueId()), btcvalue);
                if(lang.contains(locale)) {
                    p.sendMessage(RealBanks.PREFIX +
                            lang.getString(locale+".ATM.success"));
                } else {
                    p.sendMessage(RealBanks.PREFIX +
                            lang.getString("en_gb.ATM.success"));
                }

                p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0F, 0.0F);
            }
            p.closeInventory();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*public void chatListener(Inventory inv, Player pl) {
        pl.closeInventory();
        pl.sendMessage(RealBanks.PREFIX + lang.getString("ATM.enteramount"));
        new TypeMoneyListener(pl, inv);
    }*/

    public void setInventory(Inventory inv) {
        current = inv;
    }

    public Inventory getInventory() {
        return current;
    }

    public void setPlayer(Player pl) {
        p = pl;
    }

    public Player getPlayer() {
        return p;
    }


}
