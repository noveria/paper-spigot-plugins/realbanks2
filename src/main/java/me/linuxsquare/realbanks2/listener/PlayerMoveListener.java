package me.linuxsquare.realbanks2.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.PlayerInventory;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class PlayerMoveListener implements Listener {

    HashMap<Player, Long> afkPlayers = new HashMap<Player, Long>();
    Date date = new Date();

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        if(!afkPlayers.containsKey(e.getPlayer())) {
            afkPlayers.put(e.getPlayer(), date.getTime());
        } else {
            
        }
    }


}
