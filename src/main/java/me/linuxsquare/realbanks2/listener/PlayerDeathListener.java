package me.linuxsquare.realbanks2.listener;

import me.linuxsquare.realbanks2.RealBanks;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

public class PlayerDeathListener implements Listener {

    private RealBanks plugin = RealBanks.getPlugin();

    private FileConfiguration conf = plugin.getConfig();

    @EventHandler
    public void onPlayerDeath(EntityDeathEvent e) {
        if (e.getEntity() instanceof Player) {
            OfflinePlayer op = (Player) e.getEntity();
            if (conf.getBoolean("Settings.LooseMoneyonDeath")) {
                if (plugin.getEconomy().getBalance(op) > conf.getDouble("Settings.MinMoneyToLoose")) {
                    switch (conf.getString("Settings.LooseOption").toLowerCase()) {
                        case "max":
                            if(plugin.getEconomy().getName().equalsIgnoreCase("novecon2") && plugin.getEconomy().hasBankSupport()) {
                                plugin.getEconomy().bankDeposit(plugin.getEconomy().getBanks().get(0), plugin.getEconomy().getBalance(op));
                            }
                            plugin.getEconomy().withdrawPlayer(op, plugin.getEconomy().getBalance(op));
                            break;
                        case "diff":
                            if(plugin.getEconomy().getName().equalsIgnoreCase("novecon2") && plugin.getEconomy().hasBankSupport()) {
                                plugin.getEconomy().bankDeposit(plugin.getEconomy().getBanks().get(0), plugin.getEconomy().getBalance(op)-conf.getDouble("Settings.MinMoneyToLoose"));
                            }
                            plugin.getEconomy().withdrawPlayer(op, plugin.getEconomy().getBalance(op)-conf.getDouble("Settings.MinMoneyToLoose"));
                            break;
                        case "custom":
                            if(plugin.getEconomy().getName().equalsIgnoreCase("novecon2") && plugin.getEconomy().hasBankSupport()) {
                                plugin.getEconomy().bankDeposit(plugin.getEconomy().getBanks().get(0), conf.getDouble("Settings.LooseCustomAmount"));
                            }
                            plugin.getEconomy().withdrawPlayer(op, conf.getDouble("Settings.LooseCustomAmount"));
                            break;
                    }
                }
            }
        }
    }


}
