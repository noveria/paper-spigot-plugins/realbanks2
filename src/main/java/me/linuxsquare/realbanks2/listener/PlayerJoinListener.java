package me.linuxsquare.realbanks2.listener;

import me.linuxsquare.realbanks2.RealBanks;
import me.linuxsquare.realbanks2.model.DatabaseModel;
import me.linuxsquare.realbanks2.model.LanguageModel;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

    private RealBanks plugin = RealBanks.getPlugin();

    private DatabaseModel dm = plugin.getDatabaseModel();

    private FileConfiguration conf = plugin.getConfig();

    private LanguageModel lm = plugin.getLanguageModel();
    private FileConfiguration lang = lm.get();

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoinEvent(PlayerJoinEvent event) {

        OfflinePlayer op = event.getPlayer();

        dm.createBankAccountRecord(event.getPlayer().getUniqueId());

        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            if (conf.getBoolean("Settings.LooseMoneyonDeath")) {
                OfflinePlayer player = event.getPlayer();
                if (plugin.getEconomy().getBalance(player) > conf.getDouble("Settings.MinMoneyToLoose")) {
                    if(lang.contains(Bukkit.getPlayer(player.getUniqueId()).getLocale().toLowerCase())) {
                        player.getPlayer().sendMessage(RealBanks.PREFIX + lang.getString(Bukkit.getPlayer(player.getUniqueId()).getLocale().toLowerCase()+".General.morethanmin")
                                .replace("%balance%", plugin.formatMoney(conf.getDouble("Settings.MinMoneyToLoose"))));
                    } else {
                        player.getPlayer().sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.morethanmin")
                                .replace("%balance%", plugin.formatMoney(conf.getDouble("Settings.MinMoneyToLoose"))));
                    }
                }
            }
        }, 40);

    }


}
