package me.linuxsquare.realbanks2;

import me.linuxsquare.realbanks2.commands.RbCommands;
import me.linuxsquare.realbanks2.commands.RbTabCompleter;
import me.linuxsquare.realbanks2.listener.*;
import me.linuxsquare.realbanks2.model.ConfigModel;
import me.linuxsquare.realbanks2.model.DatabaseModel;
import me.linuxsquare.realbanks2.model.InterestModel;
import me.linuxsquare.realbanks2.model.LanguageModel;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.UUID;
import java.util.logging.Logger;

/*
    RealBanks2
    Copyright (C) 2021  LinuxSquare

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to contact me:
    E-Mail: linuxsquare@noveria.org
 */

 /*
=============================================================================================================

                        DISCLAIMER
    If you purchased this plugin from anywhere other than Spigot or Bukkit, 
    I regret that you were ripped off.
    Unfortunately, I can't do anything about that, 
    as the plugin is released under the GNU AGPLv3 and anyone is free (as in freedom) 
    to take the plugin and sell it, redistribute it, etc.

    However, if the purchase page did not mention a direct link to this or the public GitLab repository, 
    I would be happy if you could send me the page where you bought the plugin.

    See contact above on how you can contact me.

    Thank you in advance
    ~ LinuxSquare

=============================================================================================================
*/

public class RealBanks extends JavaPlugin {

    private static final Logger log = Logger.getLogger("Minecraft");
    private Economy econ = null;

    private static RealBanks plugin;

    private ConfigModel cm;
    private LanguageModel lm;
    private DatabaseModel dm;
    private InterestModel im;

    private DecimalFormat moneyFomat;

    public static final String PREFIX = "§9[RealBanks2] §r";


    public RealBanks() {
        plugin = this;
        this.cm = new ConfigModel(this);
        this.lm = new LanguageModel(this);
        this.dm = new DatabaseModel(this);
        this.im = new InterestModel(this);
    }

    public void onEnable() {
        PluginManager pm = this.getServer().getPluginManager();

        if (!setupEconomy()) {
            log.severe(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        this.moneyFomat = new DecimalFormat("###,##0.00");
        moneyFomat.setRoundingMode(RoundingMode.FLOOR);

        cm.loadConfig();
        dm.connect();
        lm.setup();
        im.init();
        im.checkBoolInterest();
        im.getInterestTimerFromDB();
        dm.DatabaseRefresher();
        getCommand("rb").setExecutor(new RbCommands());
        getCommand("rb").setTabCompleter(new RbTabCompleter());
        pm.registerEvents(new ATMGuiListener(), this);
        pm.registerEvents(new ATMItemClickListener(), this);
        pm.registerEvents(new CreateATMSignListener(), this);
        pm.registerEvents(new PlayerJoinListener(), this);
        pm.registerEvents(new TypeMoneyListener(), this);
        pm.registerEvents(new PlayerDeathListener(), this);

    }

    @Override
    public void onDisable() {
        dm.stopDBRefresher();
        im.stopScheduler();
        im.saveInterestTimerToDB();
        dm.close();
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    public String formatMoney(double amount) {
        String formated = moneyFomat.format(amount);
        if(amount == 1) {
            if(getConfig().getString("Settings.currencypos").equalsIgnoreCase("front") && getConfig().getString("Settings.currencypos") != null) {
                return getConfig().getString("Settings.currency") + " " + formated;
            }
            return formated + " " + getConfig().getString("Settings.currency");
        }
        return formated + " " + getConfig().getString("Settings.currency");
    }

    public void depositMoneyToBank(UUID puid, String money) {
        try {
            Statement stmt = dm.getCon().createStatement();
            stmt.executeUpdate("UPDATE Accounts SET money = " + new BigDecimal(money) + " WHERE uuid = '" + puid + "';");
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public void withdrawMoneyFromBank(UUID puid, String money) {
        try {
            Statement stmt = dm.getCon().createStatement();
            stmt.executeUpdate("UPDATE Accounts SET money = " + new BigDecimal(money) + " WHERE uuid = '" + puid + "';");
        } catch(SQLException e) {
            e.printStackTrace();
        }

    }

    public Economy getEconomy() {
        return this.econ;
    }

    public LanguageModel getLanguageModel() {
        return this.lm;
    }

    public DatabaseModel getDatabaseModel() {
        return this.dm;
    }

    public InterestModel getInterestModel() {
        return this.im;
    }

    public static RealBanks getPlugin() {
        return plugin;
    }
}
