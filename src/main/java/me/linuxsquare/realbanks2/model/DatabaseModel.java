package me.linuxsquare.realbanks2.model;

import me.linuxsquare.realbanks2.RealBanks;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.sql.*;
import java.util.HashMap;
import java.util.UUID;

public class DatabaseModel {

    private RealBanks plugin;

    private Connection con;
    private BukkitTask task;

    public DatabaseModel(RealBanks plugin) {
        this.plugin = plugin;
    }

    public void connect() {
        try {
            if(plugin.getConfig().getString("Database.type").equalsIgnoreCase("sqlite")) {
                File pluginFolder = new File("plugins" + System.getProperty("file.separator") + plugin.getDescription().getName());
                if(!pluginFolder.exists()) {
                    pluginFolder.mkdir();
                }

                File dbfile = new File(pluginFolder + System.getProperty("file.separator") + "realbanks.db");

                if(!dbfile.exists() || dbfile.length() <= 0) {
                    Bukkit.getConsoleSender().sendMessage(RealBanks.PREFIX + ChatColor.YELLOW + "Database realbanks not found. Creating...");
                    Class.forName("org.sqlite.JDBC");
                    setCon(DriverManager.getConnection("jdbc:sqlite:" + dbfile));
                    init();
                } else {
                    Class.forName("org.sqlite.JDBC");
                    setCon(DriverManager.getConnection("jdbc:sqlite:" + dbfile));
                    Bukkit.getConsoleSender().sendMessage(RealBanks.PREFIX + ChatColor.GREEN + "Database realbanks found. Hooking into it...");
                }
            } else if(plugin.getConfig().getString("Database.type").equalsIgnoreCase("mysql")) {
                String host = plugin.getConfig().getString("Database.host");
                String port = plugin.getConfig().getString("Database.port");
                String dbname = plugin.getConfig().getString("Database.DBName");
                String username = plugin.getConfig().getString("Database.username");
                String password = plugin.getConfig().getString("Database.password");

                if(username.equalsIgnoreCase("<YourDBUserHere>") || username.equalsIgnoreCase("<YourDBPasswordHere>")) {
                    Bukkit.getConsoleSender().sendMessage(RealBanks.PREFIX + ChatColor.YELLOW + "Please update your database credentials and then restart your server.");
                    Bukkit.getPluginManager().disablePlugin(plugin);
                    return;
                }

                String dburl = "jdbc:mysql://" + host + ":" + port;

                Class.forName("com.mysql.jdbc.Driver");
                setCon(DriverManager.getConnection(dburl,username,password));

                Statement stmt = getCon().createStatement();
                String sql = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '"+ dbname +"';";
                boolean exist = stmt.executeQuery(sql).next();
                if(!exist) {
                    Bukkit.getConsoleSender().sendMessage(RealBanks.PREFIX + ChatColor.YELLOW + "Database "+ dbname +" not found. Creating...");
                    String sqlq = "CREATE DATABASE `" + dbname + "`;";
                    stmt.execute(sqlq);
                } else {
                    Bukkit.getConsoleSender().sendMessage(RealBanks.PREFIX + ChatColor.GREEN + "Database "+dbname+" found. Hooking into it...");
                }
                getCon().close();

                dburl = "jdbc:mysql://" + host + ":" + port + "/" + dbname;

                Class.forName("com.mysql.jdbc.Driver");
                setCon(DriverManager.getConnection(dburl,username,password));
                exist = getCon().createStatement().executeQuery("SELECT * FROM information_schema.tables WHERE table_schema = '"+dbname+"' AND table_name = 'Accounts' LIMIT 1;").next();
                if(!exist) {
                    Bukkit.getConsoleSender().sendMessage(RealBanks.PREFIX + ChatColor.YELLOW + "Initializing Database "+ dbname);
                    init();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void close() {
        try {
            getCon().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void init() {
        try {
            Statement stmt = getCon().createStatement();

            String sql = "CREATE TABLE IF NOT EXISTS Accounts (uuid VARCHAR(40) PRIMARY KEY, money DECIMAL(65,2) NOT NULL DEFAULT 0.00);";
            stmt.executeUpdate(sql);
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS Times (uuid VARCHAR(40) PRIMARY KEY, time INT NOT NULL);");
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createBankAccountRecord(UUID puid) {
        try {
            Statement stmt = getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT uuid FROM Accounts WHERE uuid = '" + puid + "';");
            if(!rs.next()) {
                PreparedStatement pstmt = getCon().prepareStatement("INSERT INTO Accounts (uuid) VALUES (?);");
                pstmt.setString(1, puid.toString());
                pstmt.executeUpdate();
                pstmt.close();
            }
            stmt.close();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public void truncateTimes() {
        try {
            Statement stmt = getCon().createStatement();
            if(plugin.getConfig().getString("Database.type").equalsIgnoreCase("sqlite")) {
                stmt.executeUpdate("DELETE FROM Times;");
            } else if(plugin.getConfig().getString("Database.type").equalsIgnoreCase("mysql")) {
                stmt.executeUpdate("TRUNCATE TABLE Times;");
            }
            stmt.close();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertInerestTimes(UUID puid, int time) {
        try {
            PreparedStatement pstmt = getCon().prepareStatement("INSERT INTO Times (uuid,time) VALUES (?,?);");
            pstmt.setString(1,puid.toString());
            pstmt.setInt(2,time);
            pstmt.executeUpdate();
            pstmt.close();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public HashMap<UUID, Integer> getInerestTimes() {
        HashMap<UUID,Integer> pat = new HashMap<>();

        try {
            String sql = "SELECT uuid, time FROM Times;";
            Statement stmt = getCon().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()) {
                pat.put(UUID.fromString(rs.getString(1)), rs.getInt(2));
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return pat;
    }

    public void DatabaseRefresher() {
        task = Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, () -> {
            try {
                String sql = "SELECT * FROM Times LIMIT 1;";
                Statement stmt = getCon().createStatement();
                stmt.execute(sql);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }, 0 , 20*30);
    }

    public void stopDBRefresher() {
        Bukkit.getScheduler().cancelTask(task.getTaskId());
    }

    public Connection getCon() {
        return this.con;
    }


    public void setCon(Connection con) {
        this.con = con;
    }


}
