package me.linuxsquare.realbanks2.model;

import me.linuxsquare.realbanks2.RealBanks;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;
import java.math.BigDecimal;
import java.sql.*;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class DatabaseAPIModel {

    private RealBanks plugin;

    private String host;
    private String port;
    private String dbname;
    private String username;
    private String password;

    private Connection con;
    private PreparedStatement pstmt;

    private String SQLStatement;

    private FileConfiguration conf;

    public DatabaseAPIModel(RealBanks plugin) {
        this.plugin = plugin;
        this.conf = this.plugin.getConfig();
        setHost(conf.getString("Database.host"));
        setPort(conf.getString("Database.port"));
        setDBName(conf.getString("Database.DBName"));
        setUsername(conf.getString("Database.username"));
        setPassword(conf.getString("Database.password"));
    }

    public void connect() {
        try {
            if(plugin.getConfig().getString("Database.type").equalsIgnoreCase("sqlite")) {

                File pluginFolder = new File("plugins" + System.getProperty("file.separator") + plugin.getDescription().getName());
                File dbfile = new File(pluginFolder + System.getProperty("file.separator") + "realbanks.db");

                Class.forName("org.sqlite.JDBC");
                setConnection(DriverManager.getConnection("jdbc:sqlite:" + dbfile));

            } else if(plugin.getConfig().getString("Database.type").equalsIgnoreCase("mysql")) {
                String dburl = "jdbc:mysql://" + getHost() + ":" + getPort() + "/" + getDBName();

                    if(username.equalsIgnoreCase("<YourDBUserHere>") || username.equalsIgnoreCase("<YourDBPasswordHere>")) {
                        Bukkit.getConsoleSender().sendMessage(ChatColor.YELLOW + "["+ plugin.getName() +"] Please update your database credentials and then restart your server.");
                        Bukkit.getPluginManager().disablePlugin(plugin);
                        return;
                    }
                    setConnection(DriverManager.getConnection(dburl,getUsername(),getPassword()));

            }
        } catch(Exception e) {
            e.printStackTrace();
        }


    }

    public void close() {
        try {
            getConnection().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Double getMoney(UUID pUid) {

        double money = 0.0;
        String query = "SELECT money FROM Accounts WHERE uuid LIKE ?";

        try {

            PreparedStatement pstmt = getConnection().prepareStatement(query);
            pstmt.setString(1,pUid.toString());

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                money = rs.getBigDecimal("money").doubleValue();
            }
            rs.close();
            pstmt.close();

        } catch (SQLException ignored) {}

        return money;

    }

    public void setMoney(UUID pUid, double balance) {
        String query = "UPDATE Accounts SET money = ? WHERE uuid = ?";
        try {
            PreparedStatement pstmt = getConnection().prepareStatement(query);
            pstmt.setBigDecimal(1, new BigDecimal(balance));
            pstmt.setString(2, pUid.toString());

            pstmt.executeUpdate();
            pstmt.close();
        } catch (SQLException ignored) {}
    }

    public ConcurrentHashMap<UUID, Double> getAllMoney() {
        ConcurrentHashMap<UUID, Double> bankContent = new ConcurrentHashMap<>();

        String query = "SELECT * FROM Accounts;";
        try {
            Statement stmt = getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next()) {
                bankContent.put(UUID.fromString(rs.getString(1)), rs.getDouble(2));
            }
            return bankContent;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bankContent;
    }

    public void setHost(String host) {
        this.host = host;
    }
    public void setPort(String port) {
        this.port = port;
    }
    public void setDBName(String dbname) {
        this.dbname = dbname;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getHost() {
        return this.host;
    }
    public String getPort() {
        return this.port;
    }
    public String getDBName() {
        return this.dbname;
    }
    public String getUsername() {
        return this.username;
    }
    public String getPassword() {
        return this.password;
    }

    public void setConnection(Connection con) {
        this.con = con;
    }
    public Connection getConnection() {
        return this.con;
    }

    public void setPreparedStatement(PreparedStatement pstmt) {
        this.pstmt = pstmt;
    }
    public PreparedStatement getPreparedStatement() {
        return this.pstmt;
    }

    public void setSQLStatement(String SQLStatement) {
        this.SQLStatement = SQLStatement;
    }
    public String getSQLStatement() {
        return this.SQLStatement;
    }

}
