package me.linuxsquare.realbanks2.model;

import me.linuxsquare.realbanks2.RealBanks;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class LanguageModel {

    private RealBanks plugin;

    private FileConfiguration cmconf;

    private File file;
    private FileConfiguration conf;

    public LanguageModel(RealBanks plugin) {
        this.plugin = plugin;
        this.cmconf = this.plugin.getConfig();

    }

    public void setup() {
        File pluginFolder = new File("plugins" + System.getProperty("file.separator") + plugin.getDescription().getName());

        file = new File(pluginFolder, "languages.yml");

        plugin.saveResource(file.getName(), true);
        conf = YamlConfiguration.loadConfiguration(file);
    }

    public FileConfiguration get() {
        return conf;
    }

    public void reload() {
        conf = YamlConfiguration.loadConfiguration(file);
    }

}
