package me.linuxsquare.realbanks2.model;

import me.linuxsquare.realbanks2.RealBanks;

import java.io.File;

public class ConfigModel {

    private RealBanks plugin;

    public ConfigModel(RealBanks plugin) {
        this.plugin = plugin;

    }

    public void loadConfig() {
        File pluginFolder = new File(
                "plugins" + System.getProperty("file.separator") + plugin.getDescription().getName());
        if(!pluginFolder.exists()) {
            pluginFolder.mkdir();
        }

        File configFile = new File(pluginFolder + System.getProperty("file.separator") + "config.yml");
        if(!configFile.exists()) {
            plugin.saveDefaultConfig();
        }

    }

}
