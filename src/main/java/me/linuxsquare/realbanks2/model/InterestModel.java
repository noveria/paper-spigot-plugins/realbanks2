package me.linuxsquare.realbanks2.model;

import me.linuxsquare.realbanks2.RealBanks;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class InterestModel {

    private RealBanks plugin;

    private FileConfiguration lang;

    private FileConfiguration conf;

    private double percent;
    private HashMap<String,Double> money = new HashMap<String,Double>();
    private HashMap<UUID,Integer> playersAndTimes = new HashMap<UUID,Integer>();
    private int taskid;

    public InterestModel(RealBanks plugin) {
        this.plugin = plugin;
    }

    public void init() {
        setLang(plugin.getLanguageModel().get());
        setConf(plugin.getConfig());
        setPercent(conf.getDouble("Settings.BankInterestPercent"));
    }

    public void setLang(FileConfiguration lang) {
        this.lang = lang;
    }

    public void setConf(FileConfiguration conf) {
        this.conf = conf;
    }

    public void payInterestToBankAccounts(UUID pUID) {
        try {
            String sql = "SELECT uuid, money FROM Accounts WHERE uuid = ?;";
            PreparedStatement pstmt = plugin.getDatabaseModel().getCon().prepareStatement(sql);
            pstmt.setString(1, pUID.toString());
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()) {
                money.put(rs.getString(1), rs.getBigDecimal(2).doubleValue());
            }
            if(money.get(pUID.toString()) != null) {
                double doubleMoneyInt = money.get(pUID.toString());



                if(doubleMoneyInt < conf.getInt("Settings.MaximumMoney") || conf.getInt("Settings.MaximumMoney") == -1 || doubleMoneyInt > 0.00) {
                    double doubleMoney = (double) (((doubleMoneyInt / 100) * getPercent()) + doubleMoneyInt);
                    pstmt = plugin.getDatabaseModel().getCon().prepareStatement("UPDATE Accounts SET money = ? WHERE uuid = ?;");
                    pstmt.setBigDecimal(1,new BigDecimal(doubleMoney));
                    pstmt.setString(2,pUID.toString());
                    pstmt.executeUpdate();
                    Player p = Bukkit.getPlayer(pUID);
                    double interestAmount = (doubleMoney / 100.0) * getPercent();

                    if(plugin.getEconomy().getName().equalsIgnoreCase("novecon2") && plugin.getEconomy().hasBankSupport()) {
                        plugin.getEconomy().bankWithdraw(plugin.getEconomy().getBanks().get(0), interestAmount);
                    }

                    if(doubleMoneyInt != 0) {
                        if(lang.contains(p.getLocale().toLowerCase())) {
                            p.sendMessage(RealBanks.PREFIX + lang.getString(p.getLocale().toLowerCase()+".General.creditinterest").replace("%balance%", plugin.formatMoney(interestAmount)));
                        } else {
                            p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.creditinterest").replace("%balance%", plugin.formatMoney(interestAmount)));
                        }
                    }

                }
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public void checkBoolInterest() {
        if(conf.getBoolean("Settings.Interest")) {
            int period = conf.getInt("Settings.Period") * 60;
            taskid = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
                @Override
                public void run() {
                    for(Player player : Bukkit.getOnlinePlayers()) {
                        if(playersAndTimes.containsKey(player.getUniqueId())) {
                            if(playersAndTimes.get(player.getUniqueId()) >= 1) {
                                playersAndTimes.put(player.getUniqueId(), playersAndTimes.get(player.getUniqueId()) -1);
                            } else {
                                if(conf.getBoolean("Settings.LooseMoneyOnDeath")) {
                                    if(plugin.getEconomy().getBalance(Bukkit.getOfflinePlayer(player.getUniqueId())) > conf.getDouble("Settings.MinMoneyToLoose")) {
                                        if(lang.contains(player.getLocale().toLowerCase())) {
                                            player.sendMessage(RealBanks.PREFIX + lang.getString(player.getLocale().toLowerCase()+".General.morethanmin").replace("%balance%", plugin.formatMoney(conf.getDouble("Settings.MinMoneyToLoose"))));
                                        } else {
                                            player.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.morethanmin").replace("%balance%", plugin.formatMoney(conf.getDouble("Settings.MinMoneyToLoose"))));
                                        }

                                    }
                                }
                                payInterestToBankAccounts(player.getUniqueId());
                                playersAndTimes.put(player.getUniqueId(), period);
                            }
                        } else {
                            playersAndTimes.put(player.getUniqueId(), period);
                        }
                    }
                }
            }, 0, 20);
        }
    }

    public void saveInterestTimerToDB() {
        plugin.getDatabaseModel().truncateTimes();
        for(Map.Entry<UUID,Integer> entry : playersAndTimes.entrySet()) {
            plugin.getDatabaseModel().insertInerestTimes(entry.getKey(), entry.getValue());
        }
    }

    public void getInterestTimerFromDB() {
        playersAndTimes = plugin.getDatabaseModel().getInerestTimes();
    }

    public int getSeconds(UUID uid) {
        return playersAndTimes.get(uid);
    }

    public void stopScheduler() {
        Bukkit.getScheduler().cancelTask(taskid);
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    public double getPercent() {
        return this.percent;
    }

}
