package me.linuxsquare.realbanks2.commands;

import me.linuxsquare.realbanks2.RealBanks;
import me.linuxsquare.realbanks2.model.DatabaseModel;
import me.linuxsquare.realbanks2.model.InterestModel;
import me.linuxsquare.realbanks2.model.LanguageModel;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RbCommands implements CommandExecutor {

    private RealBanks plugin = RealBanks.getPlugin();

    private LanguageModel lm = plugin.getLanguageModel();
    private FileConfiguration lang = lm.get();

    private FileConfiguration conf = plugin.getConfig();

    private DatabaseModel dm = plugin.getDatabaseModel();

    private InterestModel im = plugin.getInterestModel();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            String locale = p.getLocale().toLowerCase();
            if (args.length >= 1) {
                switch (args[0].toLowerCase()) {
                    case "help":
                        if (p.hasPermission("rb.help") || p.hasPermission("rb.*")) {
                            if (args.length != 1) {
                                if(lang.contains(locale)) {
                                    p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.pleaseusehelp"));
                                } else {
                                    p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.pleaseusehelp"));
                                }
                                return true;
                            }
                            bankHelp(p);
                        } else {
                            if(lang.contains(locale)) {
                                p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.nopermission"));
                            } else {
                                p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb,General.Errors.nopermission"));
                            }
                        }
                        break;
                    case "balance":
                    case "bal":

                        if(args.length > 1 && (p.hasPermission("rb.admin.balanceothers") || p.hasPermission("rb.admin.*"))) {
                            if(args.length != 2) {
                                if(lang.contains(locale)) {
                                    p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.pleaseusehelp"));
                                } else {
                                    p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.pleaseusehelp"));
                                }
                                return true;
                            }

                            if(Bukkit.getPlayerUniqueId(args[1]) == null || !Bukkit.getOfflinePlayer(Bukkit.getPlayerUniqueId(args[1])).hasPlayedBefore()) {
                                if(lang.contains(locale)) {
                                    p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.neverplayed").replace("%player%", args[1]));
                                } else {
                                    p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.neverplayed").replace("%player%", args[1]));
                                }
                                return true;
                            }
                            double bal = bankBalanceOther(Bukkit.getOfflinePlayer(Bukkit.getPlayerUniqueId(args[1])));
                            if(lang.contains(locale)) {
                                p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.balancecheckother").replace("%balance%", plugin.formatMoney(bal)).replace("%player%", args[1]));
                            } else {
                                p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.balancecheckother").replace("%balance%", plugin.formatMoney(bal)).replace("%player%", args[1]));
                            }
                            return false;
                        }

                        if (p.hasPermission("rb.balance") || p.hasPermission("rb.*")) {
                            if (args.length != 1) {
                                if(lang.contains(locale)) {
                                    p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.pleaseusehelp"));
                                } else {
                                    p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.pleaseusehelp"));
                                }
                                return true;
                            }
                            double bal = bankBalance(p);
                            if(lang.contains(locale)) {
                                p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.balancecheck").replace("%balance%", plugin.formatMoney(bal)));
                            } else {
                                p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.balancecheck").replace("%balance%", plugin.formatMoney(bal)));
                            }
                        } else {
                            if(lang.contains(locale)) {
                                p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.nopermission"));
                            } else {
                                p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.nopermission"));
                            }

                        }
                        break;
                    case "time":
                        if (p.hasPermission("rb.time") || p.hasPermission("rb.*")) {
                            if (args.length != 1) {
                                if(lang.contains(locale)) {
                                    p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.pleaseusehelp"));
                                } else {
                                    p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.pleaseusehelp"));
                                }
                                return true;
                            }
                            interestTimer(p);
                        } else {
                            if(lang.contains(locale)) {
                                p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.nopermission"));
                            } else {
                                p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb,General.Errors.nopermission"));
                            }

                        }
                        break;
                    case "transfer":
                    case "trans":
                        if (p.hasPermission("rb.transfer") || p.hasPermission("rb.*")) {
                            if (args.length != 3) {
                                if(lang.contains(locale)) {
                                    p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.pleaseusehelp"));
                                } else {
                                    p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.pleaseusehelp"));
                                }
                                return true;
                            }
                            transferMoney(args[1], p, args[2]);
                        } else {
                            if(lang.contains(locale)) {
                                p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.nopermission"));
                            } else {
                                p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.nopermission"));
                            }

                        }
                        break;
                    case "withdraw":
                        if(p.hasPermission("rb.admin.withdraw") || p.hasPermission("rb.admin.*") || p.hasPermission("rb.*")) {
                            if(args.length != 3) {
                                if(lang.contains(locale)) {
                                    p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.pleaseusehelp"));
                                } else {
                                    p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.pleaseusehelp"));
                                }
                                return true;
                            }
                            withdrawMoney(args[1], p, args[2]);
                        } else {
                            if(lang.contains(locale)) {
                                p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.nopermission"));
                            } else {
                                p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.nopermission"));
                            }
                        }
                        break;
                    case "deposit":
                        if(p.hasPermission("rb.admin.deposit") || p.hasPermission("rb.admin.*") || p.hasPermission("rb.*")) {
                            if(args.length != 3) {
                                if(lang.contains(locale)) {
                                    p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.pleaseusehelp"));
                                } else {
                                    p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.pleaseusehelp"));
                                }
                                return true;
                            }
                            depositMoney(args[1], p, args[2]);
                        } else {
                            if(lang.contains(locale)) {
                                p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.nopermission"));
                            } else {
                                p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.nopermission"));
                            }
                        }
                        break;
                    default:
                        if(lang.contains(locale)) {
                            p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.unknowncommand"));
                        } else {
                            p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.unknowncommand"));
                        }
                        break;
                }
            } else {
                if(lang.contains(locale)) {
                    p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.pleaseusehelp"));
                } else {
                    p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.pleaseusehelp"));
                }
            }
        } else {
            sender.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.executableasplayer"));
        }
        return false;
    }

    private void bankHelp(Player pl) {
        if(pl.hasPermission("rb.admin.*") || pl.hasPermission("rb.*")) {
            pl.sendMessage(
                    "§7=== §cRealBanks Man Page §7===\n"
                            + "§c/rb help §7- shows this little man page\n"
                            + "§c/rb {bal/balance} §7- shows your current account balance\n"
                            + "§c/rb time §7- shows your current time until next interest\n"
                            + "§c/rb transfer {player} {amount} §7- transfers money from your account to another players account\n"
                            + "§c/rb withdraw {player} {amount} §7- removes the specified amount from the players bank-account\n"
                            + "§c/rb deposit {player} {amount} §7- deposits the specified amount onto the player bank-account"
            );
        } else {
            pl.sendMessage(
                    "§7=== §cRealBanks Man Page §7===\n"
                            + "§c/rb help §7- shows this little man page\n"
                            + "§c/rb {bal/balance} §7- shows your current account balance\n"
                            + "§c/rb time §7- shows your current time until next interest\n"
                            + "§c/rb transfer {player} {amount} §7- transfers money from your account to another players account"
            );
        }

    }

    private double bankBalance(Player pl) {
        double money = 0.0;
        try {
            PreparedStatement pstmt = dm.getCon().prepareStatement("SELECT money FROM Accounts WHERE uuid = ?;");
            pstmt.setString(1, pl.getUniqueId().toString());
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                money = rs.getBigDecimal(1).doubleValue();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return money;
    }

    private double bankBalanceOther(OfflinePlayer target) {
        double money = 0.0;
        try {
            PreparedStatement pstmt = dm.getCon().prepareStatement("SELECT money FROM Accounts WHERE uuid = ?;");
            pstmt.setString(1, target.getUniqueId().toString());

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                money = rs.getBigDecimal(1).doubleValue();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return money;
    }

    private void interestTimer(Player pl) {
        int seconds = im.getSeconds(pl.getUniqueId());
        int mins = 0;
        if (seconds > 59) {
            mins = ((seconds / 60) % 60);
            seconds = seconds % 60;
        }
        String locale = pl.getLocale().toLowerCase();
        if(lang.contains(locale)) {
            pl.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.interesttime")
                    .replace("%minutes%", Integer.toString(mins)).replace("%seconds%", Integer.toString(seconds)));
        } else {
            pl.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.interesttime")
                    .replace("%minutes%", Integer.toString(mins)).replace("%seconds%", Integer.toString(seconds)));
        }

    }

    private void withdrawMoney(String target, Player p, String amount) {
        String locale = p.getLocale().toLowerCase();

        try {

            if(Bukkit.getPlayerUniqueId(target) == null || !Bukkit.getOfflinePlayer(Bukkit.getPlayerUniqueId(target)).hasPlayedBefore()) {
                if(lang.contains(locale)) {
                    p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.neverplayed").replace("%player%", target));
                } else {
                    p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.neverplayed").replace("%player%", target));
                }
                return;
            }

            UUID tuid = Bukkit.getOfflinePlayer(Bukkit.getPlayerUniqueId(target)).getUniqueId();

            if(amount == null || amount.isEmpty()) {
                if(lang.contains(locale)) {
                    p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".ATM.Errors.invalidamount"));
                } else {
                    p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.ATM.Errors.invalidamount"));
                }
                return;
            }

            if(!isInteger(amount)) {
                if(!isDouble(amount)) {
                    if(lang.contains(locale)) {
                        p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".ATM.Errors.onlynumeric"));
                    } else {
                        p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.ATM.Errors.onlynumeric"));
                    }
                    return;
                }
            }

            try {
                PreparedStatement pstmt = dm.getCon().prepareStatement("SELECT money FROM Accounts WHERE uuid = ?;");
                pstmt.setString(1, tuid.toString());

                ResultSet rs = pstmt.executeQuery();

                double tmoney = 0.0;
                while (rs.next()) {
                    tmoney = rs.getBigDecimal(1).doubleValue();
                }

                if(tmoney < Double.parseDouble(amount)) {
                    if(lang.contains(locale)) {
                        p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.playernotenoughmoney").replace("%player%", target));
                    } else {
                        p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.playernotenoughmoney").replace("%player%", target));
                    }
                    return;
                }


                PreparedStatement withdraw = dm.getCon().prepareStatement("UPDATE Accounts SET money = ? WHERE uuid = ?;");
                withdraw.setBigDecimal(1, new BigDecimal(tmoney-Double.parseDouble(amount)));
                withdraw.setString(2, tuid.toString());

                withdraw.executeUpdate();
                withdraw.close();

                if(plugin.getEconomy().getName().equalsIgnoreCase("novecon2") && plugin.getEconomy().hasBankSupport()) {
                    plugin.getEconomy().bankDeposit(plugin.getEconomy().getBanks().get(0), Double.parseDouble(amount));
                }

                if(lang.contains(locale)) {
                    p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.withdrawcomplete").replace("%player%", target).replace("%amount%", plugin.formatMoney(Double.parseDouble(amount))));
                } else {
                    p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.withdrawcomplete").replace("%player%", target).replace("%amount%", plugin.formatMoney(Double.parseDouble(amount))));
                }

                pstmt.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }

        } catch (NullPointerException e) {
            if(lang.contains(locale)) {
                p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.unknownplayer"));
            } else {
                p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.unknownplayer"));
            }
        }
    }

    private void depositMoney(String target, Player p, String amount) {
        String locale = p.getLocale().toLowerCase();

        try {

            if(Bukkit.getPlayerUniqueId(target) == null || !Bukkit.getOfflinePlayer(Bukkit.getPlayerUniqueId(target)).hasPlayedBefore()) {
                if(lang.contains(locale)) {
                    p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.neverplayed").replace("%player%", target));
                } else {
                    p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.neverplayed").replace("%player%", target));
                }
                return;
            }

            UUID tuid = Bukkit.getOfflinePlayer(Bukkit.getPlayerUniqueId(target)).getUniqueId();

            if(amount == null || amount.isEmpty()) {
                if(lang.contains(locale)) {
                    p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".ATM.Errors.invalidamount"));
                } else {
                    p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.ATM.Errors.invalidamount"));
                }
                return;
            }

            if(!isInteger(amount)) {
                if(!isDouble(amount)) {
                    if(lang.contains(locale)) {
                        p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".ATM.Errors.onlynumeric"));
                    } else {
                        p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.ATM.Errors.onlynumeric"));
                    }
                    return;
                }
            }

            try {
                PreparedStatement pstmt = dm.getCon().prepareStatement("SELECT money FROM Accounts WHERE uuid = ?;");
                pstmt.setString(1, tuid.toString());

                ResultSet rs = pstmt.executeQuery();

                double tmoney = 0.0;
                while (rs.next()) {
                    tmoney = rs.getBigDecimal(1).doubleValue();
                }

                if(plugin.getEconomy().getName().equalsIgnoreCase("novecon2") && plugin.getEconomy().hasBankSupport()) {
                    if(plugin.getEconomy().bankBalance(plugin.getEconomy().getBanks().get(0)).balance < Double.parseDouble(amount)) {
                        if(lang.contains(locale)) {
                            p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".NovEcon2.Errors.centralbanknotenoughmoney"));
                        } else {
                            p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.NovEcon2.Errors.centralbanknotenoughmoney"));
                        }
                        return;
                    }
                }

                PreparedStatement deposit = dm.getCon().prepareStatement("UPDATE Accounts SET money = ? WHERE uuid = ?;");
                deposit.setBigDecimal(1, new BigDecimal(tmoney+Double.parseDouble(amount)));
                deposit.setString(2, tuid.toString());

                deposit.executeUpdate();
                deposit.close();

                if(plugin.getEconomy().getName().equalsIgnoreCase("novecon2") && plugin.getEconomy().hasBankSupport()) {
                    plugin.getEconomy().bankWithdraw(plugin.getEconomy().getBanks().get(0), Double.parseDouble(amount));
                }

                if(lang.contains(locale)) {
                    p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.depositcomplete").replace("%player%", target).replace("%amount%", plugin.formatMoney(Double.parseDouble(amount))));
                } else {
                    p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.depositcomplete").replace("%player%", target).replace("%amount%", plugin.formatMoney(Double.parseDouble(amount))));
                }

                pstmt.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }

        } catch (NullPointerException e) {
            if(lang.contains(locale)) {
                p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.unknownplayer"));
            } else {
                p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.unknownplayer"));
            }
        }
    }

    private void transferMoney(String target, Player p, String amount) {

        String locale = p.getLocale().toLowerCase();

        try {

            if(Bukkit.getPlayerUniqueId(target) == null || !Bukkit.getOfflinePlayer(Bukkit.getPlayerUniqueId(target)).hasPlayedBefore()) {
                if(lang.contains(locale)) {
                    p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.neverplayed").replace("%player%", target));
                } else {
                    p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.neverplayed").replace("%player%", target));
                }
                return;
            }

            UUID tuid = Bukkit.getPlayer(target).getUniqueId();
            UUID puid = p.getUniqueId();

            if (amount == null || amount.isEmpty()) {
                if(lang.contains(locale)) {
                    p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".ATM.Errors.invalidamount"));
                } else {
                    p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.ATM.Errors.invalidamount"));
                }
                return;
            }
            if(!isInteger(amount)) {
                if(!isDouble(amount)) {
                    if(lang.contains(locale)) {
                        p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".ATM.Errors.onlynumeric"));
                    } else {
                        p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.ATM.Errors.onlynumeric"));
                    }
                    return;
                }
            }

            if (tuid.equals(puid)) {
                if(lang.contains(locale)) {
                    p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.noselftransfer"));
                } else {
                    p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.noselftransfer"));
                }
                return;
            }

            try {

                // TODO: Gigantic security flaw! Not using Prepared Statements!

                PreparedStatement pstmtstmt = dm.getCon().prepareStatement("SELECT money FROM Accounts WHERE uuid = ?;");
                pstmtstmt.setString(1, tuid.toString());

                ResultSet rs = pstmtstmt.executeQuery();

                double tmoney = 0.0;
                while (rs.next()) {
                    tmoney = rs.getBigDecimal(1).doubleValue();
                }

                pstmtstmt = dm.getCon().prepareStatement("SELECT money FROM Accounts WHERE uuid = ?;");
                pstmtstmt.setString(1, puid.toString());
                rs = pstmtstmt.executeQuery();

                double pmoney = 0.0;
                while (rs.next()) {
                    pmoney = rs.getBigDecimal(1).doubleValue();
                }

                if (pmoney < Double.parseDouble(amount)) {
                    if(lang.contains(locale)) {
                        p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".ATM.Errors.notenoughmoney"));
                    } else {
                        p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.ATM.Errors.notenoughmoney"));
                    }

                } else {
                    tmoney += Double.parseDouble(amount);
                    pmoney -= Double.parseDouble(amount);

                    String sql = "SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;"
                            + "START TRANSACTION;"
                            + "UPDATE Accounts SET money = ? WHERE uuid = ?;"
                            + "UPDATE Accounts SET money = ? WHERE uuid = ?;"
                            + "COMMIT;";

                    PreparedStatement pstmt = dm.getCon().prepareStatement(sql);
                    pstmt.setBigDecimal(1, new BigDecimal(pmoney));
                    pstmt.setString(2,tuid.toString());
                    pstmt.setBigDecimal(3, new BigDecimal(tmoney));
                    pstmt.setString(4,puid.toString());

                    pstmt.executeUpdate();

                    if(lang.contains(locale)) {
                        p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".ATM.success"));
                    } else {
                        p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.ATM.success"));
                    }

                    String targetlocale = Bukkit.getPlayer(tuid).getLocale().toLowerCase();
                    if(lang.contains(targetlocale)) {
                        Bukkit.getPlayer(tuid).sendMessage(lang.getString(targetlocale+".General.receivetransfer")
                                .replace("%balance%", plugin.formatMoney(Double.parseDouble(amount)))
                                .replace("%player%", p.getName()));
                    } else {
                        Bukkit.getPlayer(tuid).sendMessage(lang.getString("en_gb.General.receivetransfer")
                                .replace("%balance%", plugin.formatMoney(Double.parseDouble(amount)))
                                .replace("%player%", p.getName()));
                    }



                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        } catch (NullPointerException e) {
            if(lang.contains(locale)) {
                p.sendMessage(RealBanks.PREFIX + lang.getString(locale+".General.Errors.unknownplayer"));
            } else {
                p.sendMessage(RealBanks.PREFIX + lang.getString("en_gb.General.Errors.unknownplayer"));
            }
        }

    }

    private boolean isDouble(String str) {
        String pattern = "\\d+\\.\\d{2}";

        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(str);
        return m.matches();
    }

    private boolean isInteger(String str) {
        String pattern = "\\d+";

        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(str);
        return  m.matches();
    }

}
