package me.linuxsquare.realbanks2;

import me.linuxsquare.realbanks2.model.ConfigModel;
import me.linuxsquare.realbanks2.model.DatabaseAPIModel;
import me.linuxsquare.realbanks2.model.LanguageModel;

/*
    RealBanks2
    Copyright (C) 2021  LinuxSquare

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    How to contact me:
    E-Mail: linuxsquare@noveria.org
 */

 /*
=============================================================================================================

                        DISCLAIMER
    If you purchased this plugin from anywhere other than Spigot or Bukkit, 
    I regret that you were ripped off.
    Unfortunately, I can't do anything about that, 
    as the plugin is released under the GNU AGPLv3 and anyone is free (as in freedom) 
    to take the plugin and sell it, redistribute it, etc.

    However, if the purchase page did not mention a direct link to this or the public GitLab repository, 
    I would be happy if you could send me the page where you bought the plugin.

    See contact above on how you can contact me.

    Thank you in advance
    ~ LinuxSquare

=============================================================================================================
*/

public class RealBanksAPI {

    private RealBanks plugin;

    private ConfigModel cm;
    private DatabaseAPIModel dapim;
    private LanguageModel lm;

    public RealBanksAPI() {
        System.out.println("Created Instance of RealBanks");
        this.plugin = RealBanks.getPlugin();
        this.cm = new ConfigModel(plugin);
        this.dapim = new DatabaseAPIModel(plugin);
        this.lm = new LanguageModel(plugin);

        getDatabaseAPIModel().connect();
    }

    public ConfigModel getConfigModel() {
        return this.cm;
    }

    public DatabaseAPIModel getDatabaseAPIModel() {
        return this.dapim;
    }

    public LanguageModel getLanguageModel() {
        return this.lm;
    }

}
