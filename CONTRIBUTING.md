# Contributing to RealBanks2
## Disclaimer
This Project underlies the GNU Affero General Public License version 3

```
    RealBanks2
    Copyright (C) 2021  LinuxSquare

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

```

## Bugfixes

If you see any bugs, that you can fix, you can either create an issue thread or directly a merge request.

## Translating

Current supported languages are:

    * Swiss German (de_ch)
    * German (de_de)
    * English Great Britain (en_gb)

You don't see your language? Don't worry, you are allowed to translate the plugin. Just make sure, that you use the [language code, listed on the minecraft gamepedia](https://minecraft.gamepedia.com/Language),
Copy any language you wan't as a base, and I beg you, please don't change the variables like `%balance%` or `%player%` otherwise the plugin won't function correctly.

However, you can place them to another position like this:
```yaml
DepositMessage: "§7You have deposited §a%balance% §7into §a%player%'s §7pocket!"
```
to
```yaml
DepositMessage: "§7You have deposited §a%player% §7into §a%balance%'s §7pocket!"
```

If you have any questions about translating this plugin, don't hesitate to contact me :)

## Features
You got any features you want to implement? Then go ahead and fork this plugin and create a merge request.
Or just publish it on you own, if you want.
