# RealBanks Plugin

**This plugin won't be available on any spigot or bukkit page, because I want to prevent unexperienced users to add Issues on a feature they don't understand.**
**To use this plugin, you have to compile it on your own using `maven`.**

_I'm sorry for this inconvenience_

## Build instructions Linux & macOS

Install maven (git optional) for your current distribution (macOS users have built-in maven support since 10.6.8 Snow Leopard).

Debian / Ubuntu:
```bash
sudo apt-get install maven git
```

Arch Linux / Manjaro / _Antergos (in loving memories)_
```bash
sudo pacman -Sy maven git
```

Fedora / Red Hat
```bash
sudo dnf install maven git
```

OpenSUSE
```bash
sudo zypper in maven git
```

Solus
```bash
sudo eopkg install apache-maven git
```


Clone this repository using
```
git clone https://gitlab.dragon-clouds.com/noveria-plugins/realbanksmvc.git
```
or download the repository as zip.



Navigate into the cloned repository using `cd RealBanks`.

Install it using `mvn install`.

Now move the `RealBanks.jar` inside the `target/` directory into the `plugins/` directory of your server.

## Build instructions Windows

**I recommend using the built-in Linux Subsystem for Windows, if you're using Windows 10.**

Install Linux Subsystem and Linux Distro:
https://docs.microsoft.com/en-us/windows/wsl/install-win10 

(Recommended: Debian or Ubuntu)

**Otherwise use this guide to install maven locally on your machine**

Install Maven locally:
https://howtodoinjava.com/maven/how-to-install-maven-on-windows/

**Now follow the steps for [compiling on Linux](#build-instructions-linux-macos)**

## Support Me
If you want to support me financially, you can do this in two ways. Either in Bitcoin or via my Ko-Fi.
If you do, I really appreciate you for supporting me and my work :)

<a href="https://ko-fi.com/linuxsquare"><img src="https://ko-fi.com/img/Kofi_Logo_Blue.svg" width="150"></img></a>

Support me on Ko-Fi

<a href="bitcoin:bc1q9ccrxefj5phkpkrset0u5ars28g6zgl8fq4t0v"><img src="https://en.bitcoin.it/w/images/en/c/cb/BC_Logotype.png" width="150"></img></a>

or via BitCoin